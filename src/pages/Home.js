import { Fragment } from 'react';
import Banner from '../components/Banner';
import React from 'react';
import { Button } from 'react-bootstrap';
import Highlights from '../components/Highlights';
import ProductCard from '../components/Product';
import { Link } from 'react-router-dom';

export default function Home() {
  const data = {
    title: "Online Store",
    content: "Shoes for everyone, everywhere",
    destination: "/products",
  };

  return (
    <Fragment>
      <div className="text-center m-2">
        <Banner data={data} />
        <Highlights />
      </div>
    </Fragment>
  );
}