import { Fragment, useEffect, useState } from 'react';
import React from 'react'

import Banner from '../components/Banner';
import AdminTable from '../components/AdminTable';
import ProductCard from '../components/Product';
import { Link } from 'react-router-dom';


export default function Admin() {

		const [products, setProducts] = useState([])

		useEffect(() => {

				fetch(`${process.env.REACT_APP_API_URL}/products/all`)
				.then(res => res.json())
				.then(data => {
				    
				    console.log(data);

				    setProducts(data.map(product => {
				        return (
				            <AdminTable key={product._id} productProp={product}/>
				        );
				    }));
				});
		    }, []);

		return(
			<Fragment>
			<div className="text-center m-2">
			  <h3>Admin Access Only Page</h3>
			  <Link className="btn btn-outline-info" to={`/admin/productCreate/products`}>
			    Add a new Product
			  </Link>
			 </div>
				{products}
			</Fragment>
		)
	}
