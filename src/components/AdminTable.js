import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';

export default function AdminTable({ productProp }) {
  const { name, description, price, isActive, _id } = productProp;

  console.log(productProp);

  return (
    <Container>
      <Row>
        <Col>
          <Card className="custom-card">
            <Card.Body>
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>₱ {price}</Card.Text>
              <Card.Subtitle>Status</Card.Subtitle>
              <Card.Text className="m-2">{isActive ? 'Item is in stock' : 'Item is not in stock'}</Card.Text>

              <div className="btn-group" role="group" aria-label="Product Actions">
                <Link className="btn btn-outline-info" to={`/admin/productUpdate/${_id}`}>
                  Edit Item
                </Link>
                <Link className="btn btn-outline-danger" to={`/admin/productArchive/${_id}/archive`}>
                  Archive Item
                </Link>
                <Link className="btn btn-outline-success" to={`/admin/productActivate/${_id}/activate`}>
                  Unarchive Item
                </Link>
              </div>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}

AdminTable.propTypes = {
  productProp: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    isActive: PropTypes.bool.isRequired,
    _id: PropTypes.string.isRequired,
  }),
};