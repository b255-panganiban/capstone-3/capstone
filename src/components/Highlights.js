import { Row, Col, Card} from 'react-bootstrap';
import shoes from '../images/shoes.jpg';
import tshirt from '../images/tshirt.jpg';
import etc from '../images/etc.jpg';

export default function Highlights(){
    return (
        <Row className="mt-3 mb-3">
        <Col xs={12} md={4}>
            <Card className="cardHighlight p-3">
              <Card.Img variant="top" src={shoes} alt="Image" />
              <Card.Body>
                <Card.Title>
                  <h2>Introducing Nike Shoes</h2>
                </Card.Title>
                <Card.Text>
                   the epitome of style and performance. Engineered with cutting-edge technology and crafted with precision, Nike Shoes offer unparalleled comfort and support for your active lifestyle. Experience the perfect blend of fashion and function as you conquer the streets, the gym, or any terrain with confidence. Elevate your game with Nike's iconic designs and unleash your potential. Step into greatness with Nike Shoes and make every stride count.
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                <Card.Img variant="top" src={tshirt} alt="Image" />
                    <Card.Body>
                        <Card.Title>
                            <h2>Nike T-Shirts</h2>
                        </Card.Title>
                        <Card.Text>
                            Discover the ultimate comfort and style with our collection of Nike T-Shirts. Designed with premium materials and innovative features, these t-shirts offer a perfect fit and unmatched durability. Whether you're hitting the gym, going for a run, or simply hanging out with friends, Nike T-Shirts provide breathability and freedom of movement. Embrace the iconic Nike logo and choose from a variety of designs and colors that suit your individual style.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                <Card.Img variant="top" src={etc} alt="Image" />
                    <Card.Body>
                        <Card.Title>
                            <h2>Accessories</h2>
                        </Card.Title>
                        <Card.Text>
                            Complete your athletic look with Nike Accessories. From stylish bags to comfortable socks, our collection offers the perfect blend of functionality and style. Carry your essentials with ease and confidence using Nike Bags designed with durable materials and convenient compartments. Keep your feet cool and supported during workouts or everyday activities with Nike Socks, featuring cushioning and moisture-wicking properties.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>

    )
}
