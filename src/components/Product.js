import { Container, Row, Col, Card, Button} from 'react-bootstrap';
import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default function ProductCard({productProp}) {

    const {name, description, price, isActive, _id} = productProp;

    return (
        <Container>
        <Card>
        <Row className="m-2 p-2">
        <Col>
            <Card.Body>
                <Card.Title><h2>{name}</h2></Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>₱ {price}</Card.Text>
                <Card.Subtitle>Status</Card.Subtitle>
                <Card.Text className="m-2">{isActive ? 'Item is in stock' : 'Item is not in stock'}</Card.Text>
                <Link className="btn btn-primary" block="true"to={`/products/${_id}`}>Details</Link>
                </Card.Body>
        </Col>
        </Row>
        </Card>
        </Container>
    )
}

ProductCard.propTypes = {
        product: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
        isActive: PropTypes.bool.isRequired,
    })
}
