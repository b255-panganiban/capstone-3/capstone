import { Fragment } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes, useNavigate } from 'react-router-dom';
import React from 'react';
import AppNavBar from './components/AppNavBar';
import Products from './pages/Products';
import ProductView from './components/ProductView';
import ProductUpdate from './components/ProductUpdate';
import ProductArchive from './components/ProductArchive';
import ProductActivate from './components/ProductActivate';
import ProductCreate from './components/ProductCreate';
import Home from './pages/Home';
import Admin from './pages/Admin';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import { useState, useEffect } from 'react';

import './App.css';
import { UserProvider } from './UserContext';

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user]);

  console.log('Initial user:', user);

  // Function to check if the user is an admin
  const isAdminUser = () => {
    return user.isAdmin === true;
  }

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <Container fluid>
          <AppNavBar />
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/products" element={<Products />} />
            <Route path="/products/:productId" element={<ProductView />} />
            {isAdminUser() && (
              <Fragment>
                <Route path="/admin" element={<Admin />} />
                <Route path="/admin/productUpdate/:productId" element={<ProductUpdate />} />
                <Route path="/admin/productArchive/:productId/archive" element={<ProductArchive />} />
                <Route path="/admin/productActivate/:productId/activate" element={<ProductActivate />} />
                <Route path="/admin/productCreate/products" element={<ProductCreate />} />
              </Fragment>
            )}
            <Route path="/register" element={<Register />} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="*" element={<Error />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;